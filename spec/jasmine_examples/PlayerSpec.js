describe('MasterController', function() {
    var MasterController,
        $rootScope,
        $scope;

    //beforeEach(module('myModule'));

    // beforeEach(inject(function($rootScope, $injector, $controller) {
    //     $rootScope = $rootScope;
    //     $scope = $rootScope.$new();
    //     MasterController = $controller('MasterController', {
    //         '$scope': $scope
    //     });
    //     $scope.$digest();
    // }));

    describe('$locationChangeSuccess event listener', function() {
        it('should set $scope.success to true', function() {
            var newUrl = 'http://foourl.com';
            var oldUrl = 'http://barurl.com'

            $scope.$apply(function() {
                $rootScope.$broadcast('$locationChangeSuccess', newUrl, oldUrl);
            });

            expect($scope.success).toBe(true);
        });
    });
});