var app = angular.module('PlanThouLife', ['ngRoute', 'ngCookies', 'mp.datePicker', 'mp.colorPicker', 'firebase']);

// for ngRoute
app.run(['$rootScope', '$location', function($rootScope, $location) {
  $rootScope.$on('$routeChangeError', function(event, next, previous, error) {
    // We can catch the error thrown when the $requireSignIn promise is rejected
    // and redirect the user back to the home page
    if (error === "AUTH_REQUIRED") {
        $location.path('/login');
      
    }
  });
}]);

app.config(function($routeProvider) {
    $routeProvider
        .when('/home', {
            controller: 'HomeController',
            templateUrl: 'views/home.html',
            resolve: {
                "currentAuth": ["Auth", function(Auth){
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'views/login.html'
        })
        .when('/create_user', {
            controller: 'LoginController',
            templateUrl: 'views/create_account.html'
        })

    //   .when('/chat', {
    //     controller: 'ChatController',
    //     templateUrl: '/views/chat.html'
    //   })

    .otherwise({
        redirectTo: '/login'
    });
});

app.factory("Auth", ["$firebaseAuth", function($firebaseAuth){
    return $firebaseAuth();
}]);