//This is the controller class that acts for the application when things such as classes and color are added.

app.controller('HomeController', ['$scope', '$location', '$route', '$firebaseAuth', '$firebaseObject', function($scope, $location, $route, $firebaseAuth, $firebaseObject) {

    //var chat_div = document.getElementById("tasks");
    $scope.task = {};
    $scope.class_to_add = {}; //Holds class to add
    $scope.classes = []; //Holds list of classes
    $scope.auth = $firebaseAuth();
    $scope.uid = $scope.auth.$getAuth().uid;
    $scope.user_data = {};
    $scope.user_data.tasks = [];

    // firebase object testing
    var ref = firebase.database().ref('users/' + $scope.uid);
    var obj = $firebaseObject(ref);
    obj.$bindTo($scope, 'user_data').then(function() {
        console.log("User data loaded.");
    })

    /////////////// add tasks /////////////////
    $scope.addTask = function() {
        console.log("in addTask()");

        if (!$scope.task.task || !$scope.task.date)
            return;

        // testing saving tasks
        if (!$scope.user_data.tasks) {
            $scope.user_data.tasks = [$scope.task];
        } else {
            $scope.user_data.tasks.push($scope.task);
        }

        //sort the tasks
        $scope.user_data.tasks.sort($scope.compareDate);

        $scope.task = {};
    };

    $scope.addClass = function() {
        if (!$scope.user_data.classes) {
            $scope.user_data.classes = [$scope.class_to_add];
        } else {
            $scope.user_data.classes.push($scope.class_to_add);
        }
        $scope.class_to_add = {};
    };

    $scope.pickClass = function(task_class) {
        $scope.task.class = task_class;
    };

    //helper function to sort by date
    $scope.compareDate = function(a, b) {
        var aDate = new Date(a.date);
        var bDate = new Date(b.date);
        if (aDate < bDate)
            return -1;
        if (aDate > bDate)
            return 1;
        return 0;

    }


    //mark a task as done
    $scope.complete_task = function(index) {
        // toggle completed property on task
        if ($scope.user_data.tasks[index].completed) {
            $scope.user_data.tasks[index].completed = false;
        } else {
            $scope.user_data.tasks[index].completed = true;
        }
    }

    //remove a task
    $scope.delete_task = function(index) {
        $scope.user_data.tasks[index].deleted = true;
    }

    $scope.logout = function() {
        $scope.auth.$signOut().then(function() {
            $location.path('/login');
        });
    };

}]);