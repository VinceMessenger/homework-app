**PlanThouLife**

Introduction:
This scheduling tool will assist in day to day life by allowing users a simple interface for scheduling their life. It uses simple MEAN.js framework and is cross compatible across devices.

Requirements:
   Up to Date Device. This includes Windows, MacOSX, and mobile platforms.

How to Access:
(1)Navigate to http://dana.ucc.nau.edu/vjm36/hw-app/#!/home to access the application. From here you will see Plan Thou Life, which is our prototype for PlanMyLife

Additional Features:
(1)You can change the layout through the event viewing frame.
(2)The event viewing frame acts as a form which will allow you create events.
(3)User Accounts have been added.
(4)Internal Tests have been created for Quality Assurance.